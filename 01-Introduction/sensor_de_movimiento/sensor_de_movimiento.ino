#define pinPIR 8
#define pinLED 13
#define MILISEGUNDOS 1000
int iCalibrar = 10;
boolean bMovimiento = true;
void LED(int iONOFF){
  switch(!iONOFF){
    case 0:
      digitalWrite(pinLED, LOW);
      delay(MILISEGUNDOS);
    break;
    case 1:
      digitalWrite(pinLED, HIGH);
      delay(MILISEGUNDOS);
    break;
  }
}
void setup() {
  Serial.begin(9600);
  pinMode(pinPIR, INPUT);
  pinMode(pinLED, OUTPUT);
  LED(0);//apagar
  Serial.print(" Esperando al sensor ");
  for(int i = 0; i < iCalibrar; i++){
    Serial.print(".");
    delay(MILISEGUNDOS);
  }
  Serial.println("INICIADO");
  delay(50);
}
void loop() {
  if(digitalRead(pinPIR) == HIGH){
    LED(1);//encender
    if(bMovimiento){
      bMovimiento = false;
      Serial.print("Se encontro movimiento\n");
      delay(100);
    }
  }
  else{
    LED(0);//apagar
    bMovimiento = true;
    delay(100);
  }
}

